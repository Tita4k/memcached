#include "protocol.h"
#include <stdexcept>

using namespace std;

MC_COMMAND CommandName2Code(const string& param) {
    if (param == "add") {
        return MC_COMMAND::CMD_ADD;
    } else if (param == "set") {
        return MC_COMMAND::CMD_SET;
    } else if (param == "get") {
        return MC_COMMAND::CMD_GET;
    } else if (param == "delete") {
        return MC_COMMAND::CMD_DELETE;
    } else if (param == "replace") {
        return MC_COMMAND::CMD_REPLACE;
    } else if (param == "touch") {
        return MC_COMMAND::CMD_TOUCH;
    } else if (param == "append") {
        return MC_COMMAND::CMD_APPEND;
    } else if (param == "prepend") {
        return MC_COMMAND::CMD_PREPEND;
    } else {
        throw std::runtime_error("exception");
    }
    //return MC_COMMAND::CMD_UNKNOWN;
}


void McCommand::Deserialize(RBuffer* buffer) {
    string cmd = buffer->ReadField(' ');
    buffer->ReadChar();
    command = CommandName2Code(cmd);
    switch (command) {
        case CMD_GET:
		{
        	   string arr = buffer->ReadField('\r') + " ";
        	   string str = "";
		   for (char ch : arr) {
		       if (ch != ' ') {
		           str += ch;
		       } else {
		           keys.push_back(str);
		           str = "";
		       }
		    }
		    buffer->ReadCharCheck('\r');
		    buffer->ReadCharCheck('\n');
		    break;
		}
        case CMD_DELETE:
        	keys.push_back(buffer->ReadField('\r'));
        	buffer->ReadCharCheck('\r');
        	buffer->ReadCharCheck('\n');
		break;
        case CMD_TOUCH:
            keys.push_back(buffer->ReadField(' '));
            buffer->ReadChar();
            exp_time = (time_t)buffer->ReadUint32();
            buffer->ReadCharCheck('\r');
            buffer->ReadCharCheck('\n');
        break;
        case CMD_APPEND:
        case CMD_PREPEND:
	    case CMD_ADD:
	    case CMD_SET:
        case CMD_REPLACE:
            keys.push_back(buffer->ReadField(' '));
        	buffer->ReadChar();
        	flags = buffer->ReadUint32();
        	buffer->ReadChar();
        	exp_time = (time_t)buffer->ReadUint32();
        	buffer->ReadChar();
        	size_t bytes = buffer->ReadUint32();
        	buffer->ReadCharCheck('\r');
        	buffer->ReadCharCheck('\n');
        	data = buffer->ReadBytes(bytes);
        	buffer->ReadCharCheck('\r');
        	buffer->ReadCharCheck('\n');
            break;
    }
}

string ResultCode2String(MC_RESULT_CODE code) {
    if (code == R_STORED) {
        return "STORED";
    } else if (code == R_NOT_STORED) {
        return "NOT_STORED";
    } else if (code == R_EXISTS) {
        return "EXISTS";
    } else if (code == R_NOT_FOUND) {
        return "NOT_FOUND";
    } else if (code == R_TOUCHED) {
        return "TOUCHED";
    }
    return "DELETED";
}

void McResult::Serialize(WBuffer* buffer) const{
    switch (type_) {
        case RT_ERROR:
            buffer->WriteField("ERROR", ' ');
            buffer->WriteField(error_message_);

            buffer->WriteChar('\r');
            buffer->WriteChar('\n');
            break;
        case RT_CODE:
            buffer->WriteField(ResultCode2String(code_));

            buffer->WriteChar('\r');
            buffer->WriteChar('\n');
		    break;
        case RT_VALUE:
            for (size_t i = 0; i < values_.size(); i++) {
                buffer->WriteField("VALUE", ' ');
                values_[i].Serialize(buffer);
            }
		    buffer->WriteField("END");
            buffer->WriteChar('\r');
            buffer->WriteChar('\n');
            break;
        }
}

void McValue::Serialize(WBuffer* buffer) const{
    buffer->WriteField(key_, ' ');
    buffer->WriteUint32(flags_);
    buffer->WriteChar(' ');
    buffer->WriteUint32(data_.size());
    buffer->WriteChar('\r');
    buffer->WriteChar('\n');
    buffer->WriteBytes(data_);
    buffer->WriteChar('\r');
    buffer->WriteChar('\n');
}
