#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <unistd.h>
#include "buffer.h"
#include "protocol.h"
#include "sever.h"

void echo(int conn_fd/*фаиловый дескриптор соединения по сокету*/, Cache *c) {
    Cache cache = *c;

    SocketRBuffer* rb = new SocketRBuffer(1, conn_fd); // буффер чтения из сокета
    SocketWBuffer* wb = new SocketWBuffer(1, conn_fd); // буффер записи в сокет
    
    while (true) {
	    McCommand cmd;
        try {
	        cmd.Deserialize(rb);
        }
        catch(...){ // при возникновении исключения (пользователем заданы неправильные параметры) отключаем соединение, сервер продолжает работать
            return;
        }
        fprintf(stderr, "cmd: %d\n", (int)cmd.command);
        switch (cmd.command) {
            case CMD_GET:
            {
                std::vector<McValue> vals;// получаем каждое значение из списка и сохраняем в массив
                for (std::string key : cmd.keys){
                    std::vector<char> data;
                    time_t exp_time;
                    time_t update_time;
                    int32_t flags;
                    bool existed = cache.get(key, data, exp_time, update_time, flags);
                    McValue val(key, flags, data);
                    if (existed) {
                        vals.push_back(val);
                    }
                }
                McResult res(vals);
                res.Serialize(wb);
                break;
            }
            case CMD_SET:
            {
                std::string key = cmd.keys[0];
                cache.set(key, cmd.exp_time, cmd.data, cmd.flags);
                McResult res(R_STORED);
                res.Serialize(wb);
                break;
            }
            case CMD_REPLACE:
            {
                std::string key = cmd.keys[0];
                bool existed = cache.exists(key);
                if (!existed) {
                    McResult res(R_NOT_STORED);
                    res.Serialize(wb);
                } else {
                    cache.set(key, cmd.exp_time, cmd.data, cmd.flags);
                    McResult res(R_STORED);
                    res.Serialize(wb);
                }      
                break;
            }
            case CMD_TOUCH:
            {
                std::string key = cmd.keys[0];
                bool existed = cache.exists(key);
                if (!existed) {
                    McResult res(R_NOT_FOUND);
                    res.Serialize(wb);
                } else {
                    std::vector<char> data;
                    time_t exp_time;
                    time_t update_time;
                    int32_t flags;
                    cache.get(key, data, exp_time, update_time, flags);
                    cache.set(key, cmd.exp_time, data, flags);
                    McResult res(R_TOUCHED);
                    res.Serialize(wb);
                }
                break;    
            }
            case CMD_APPEND:
            {
                std::string key = cmd.keys[0];
                bool existed = cache.exists(key);
                if (!existed) {
                    McResult res(R_NOT_STORED);
                    res.Serialize(wb);
                } else {
                    cache.append(key, cmd.data);
                    McResult res(R_STORED);
                    res.Serialize(wb);
                }
                break;
            }
            case CMD_PREPEND:
            {
                std::string key = cmd.keys[0];
                bool existed = cache.exists(key);
                if (!existed) {
                    McResult res(R_NOT_STORED);
                    res.Serialize(wb);
                } else {
                    cache.prepend(key, cmd.data);
                    McResult res(R_STORED);
                    res.Serialize(wb);
                }
                break;
            }
            case CMD_ADD:
            {
                std::string key = cmd.keys[0];
                bool existed = cache.exists(key);
                if (existed) {
                    McResult res(R_NOT_STORED);
                    res.Serialize(wb);
                } else {
                    cache.set(key, cmd.exp_time, cmd.data, cmd.flags);
                    McResult res(R_STORED);
                    res.Serialize(wb);
                }
                break;
            }
            case CMD_DELETE:
            {
                std::string key = cmd.keys[0];
                bool existed = cache.del(key);
                if (existed) {
                    McResult res(R_DELETED);
                    res.Serialize(wb);
                } else {
                    McResult res(R_NOT_FOUND);
                    res.Serialize(wb);
                }
                break;
            }
            case CMD_UNKNOWN:
                break;
            case MC_COMMANDS_NUMBER:
                break;
        }

	    // get result from command

	    //McResult res;
        
	    //res.Serialize(wb)

        sleep(0.1);
    }
}

int main(int argc, char** argv) {
    fprintf(stderr, "starting server %d: %s, %s\n", argc, argv[0], argv[1]); // конфиг: первым параметром передается порт, вторым размер кэша.
    const int backlog = 13;

    int status;
    struct addrinfo hints;
    struct addrinfo *servinfo;       // will point to the results
    char* port = argv[1];// порт
    int cache_size = (argc > 2) ? atoi(argv[2]) : 400; // считывает порт и размер кэша из командной строки
    /*
        if (argc > 2)
            cache_size = argv[2];
        else
            cache_size = 400;
    */

    memset(&hints, 0, sizeof hints); // make sure the struct is empty
    hints.ai_family = AF_INET;       // IPv4
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me

    if ((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror    (status));
        exit(1);
    }


    int sd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (sd == -1) {
        fprintf(stderr, "Error, socket"); 
        exit(1);
    }
    if (bind(sd, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
        fprintf(stderr, "Error, bind");
        exit(1);
    }
    if (listen(sd, backlog) == -1) {
        fprintf(stderr, "Error, listen");
        exit(1);
    }
    freeaddrinfo(servinfo); // free the linked-list


    struct sockaddr_storage conn_addr;
    socklen_t addr_size = sizeof conn_addr;
    int conn_fd = 0;
    Cache *cache = new Cache();
    cache->MAX_SIZE = cache_size;
    while (true) {
        conn_fd = accept(sd, (struct sockaddr *)&conn_addr, &addr_size);
        fprintf(stderr, "connection accepted \n");    

		echo (conn_fd, cache);

    }

    return 0;
}


