#include <map>
#include <limits>

typedef std::string Key;
typedef int32_t Flags;
typedef std::vector<char> Value;
typedef time_t Time;


class Cache {
private:
    std::map<Key, Value> table;
    std::map<Key, Flags> flagg;
    std::map<Key, Time> e_times;
    std::map<Key, Time> u_times;
public:
    unsigned int MAX_SIZE = 400;
    bool exists(Key key) {
        return table.count(key) > 0;
    }
    Time get_time(){
        Time* t;
        time(t);
        return *t;
    }
    void clean() {
        Time currentTime = get_time();
        Time min = std::numeric_limits<int>::max();
        Key key;
        bool deleted = false; 
        for (auto const &iter : u_times) {
            Key k = iter.first;
            Time t = iter.second;
            if (t < min) {
                key = k;
                min = t;
            }
            Time e = e_times[k];
            if (e > 0 && e < currentTime) { // удаляет старые ключи.
                del(key);
                deleted = true;
            }
        }
        if (!deleted)
            del(key);
    }
    bool get(Key key, Value &value, Time &exp_time, Time &update_time, Flags &flags){
        // 1. проверить, есть ли в таблице значение по этому ключу (стандартной функцией)
        // 2. если значения нет, вернуть false
        if (!exists(key))
            return false;

        Value val = table[key];
        value = val;
        exp_time = e_times[key];
        flags = flagg[key];
        if ((exp_time > 0 && exp_time < get_time()) || (exp_time <= 0)) {
            del(key);
            return false;
        }
        // 4. проверить exp_time и удалить ключ, если закончилось время хранения
        // 5. записать в exp_time и update_time нужные значения
        // 3. вернуть true
        return true;
    }
    bool set(Key key, Time exp_time, Value value, Flags flags){
        bool existed = exists(key);
        if (!existed && table.size() >= MAX_SIZE){
            clean();
        }
        table[key] = value;
        e_times[key] = exp_time;
        
        Time t = get_time() - 86400; // если время больше чем на сутки уходит в прошлое, то считаем его относительным.
        if (exp_time > 0 && exp_time < t)
            e_times[key] = get_time() + exp_time;
        
        u_times[key] = get_time();
        flagg[key] = flags;
        return existed;

        // 1. добавить сохранение exp_time
        // 2. получить текущее время
        // 3. сохранить текущее время как update_time на будущее
    }
    void append(Key key, Value value){        
        Value val = table[key];
        val.insert(val.end(), value.begin(), value.end());
        table[key] = val;
    }
    void prepend(Key key, Value value){
        Value val = table[key];
        value.insert(value.end(), val.begin(), val.end());
        table[key] = value;
    }
    bool del(Key key){
        bool existed = exists(key);
        if (!existed) {
            return false;
        }
        table.erase(key);
        e_times.erase(key);
        u_times.erase(key);
        flagg.erase(key);
        return true;
        // 1. проверить, есть ли в таблице значение по этому ключу (стандартной функцией)
        // 2. если значения нет, вернуть false
    }
};
