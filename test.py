import socket
import sys
import time
import subprocess

port = 3249

mc = subprocess.Popen(['./project/server', str(port)])
time.sleep(3)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("localhost", port))

def check_echo(msg, result):
    print("starting: " + msg)
    s.send(msg)
    print("expect: " + str([result]))

    data = ""
    delta = len(result) - len(data)
    while (delta > 0):
        data += s.recv(delta)
        delta = len(result) - len(data)
        print("chunk: " + data)
    #s.shutdown(socket.SHUT_WR)  # break reading cycle in echo server

    res = data
    print("got: " + str([res]))

    assert res == result


def test_set():
    msg = "set abc 1 "+ft+" 5\r\nHello\r\n"
    check_echo(msg, "STORED\r\n")

def test_time():
    t = time.time() - 5
    tt = int(t)
    ttt = str(tt)
    check_echo("set xrt 1 "+ttt+" 5\r\nHello\r\n", "STORED\r\n")
    check_echo("get xrt\r\n", "END\r\n")

def test_time_2():
    t = time.time() + 5
    tt = int(t)
    ttt = str(tt)
    check_echo("set xit 1 "+ttt+" 5\r\nHello\r\n", "STORED\r\n")
    time.sleep(6)
    check_echo("get xit\r\n", "END\r\n")



if __name__ == "__main__":
    ft = str(int(time.time() + 1000));
    port = int(sys.argv[1])
    check_echo("get abc\r\n", "END\r\n")
    test_set()
    test_set()
    check_echo("get abc\r\n", "VALUE abc 1 5\r\nHello\r\nEND\r\n")
    check_echo("get abc xyz\r\n", "VALUE abc 1 5\r\nHello\r\nEND\r\n")
    check_echo("add abc 1 "+ft+" 9\r\nHello1234\r\n", "NOT_STORED\r\n")
    check_echo("add xxx 2 123456789 6\r\nHello1\r\n", "STORED\r\n")
    check_echo("touch xxx 123456\r\n", "TOUCHED\r\n")
    check_echo("add xyz 1 1242 9\r\nHello1234\r\n", "STORED\r\n")
    check_echo("get abc xyz\r\n", "VALUE abc 1 5\r\nHello\r\nVALUE xyz 1 9\r\nHello1234\r\nEND\r\n")
    check_echo("append xyz 1 1242 2\r\nHe\r\n", "STORED\r\n")
    check_echo("prepend xyz 1 1242 2\r\nLL\r\n", "STORED\r\n")
    check_echo("get xyz\r\n", "VALUE xyz 1 13\r\nLLHello1234He\r\nEND\r\n")
    check_echo("delete xyz\r\n", "DELETED\r\n")
    check_echo("get xyz\r\n", "END\r\n")
    check_echo ("touch xyz 12345\r\n", "NOT_FOUND\r\n")
    check_echo("delete xyz\r\n", "NOT_FOUND\r\n")
    check_echo("get xyz\r\n", "END\r\n")
    check_echo ("replace abc 1 "+ft+" 9\r\nHello1234\r\n", "STORED\r\n")
    check_echo ("get abc\r\n", "VALUE abc 1 9\r\nHello1234\r\nEND\r\n")
    check_echo ("replace xyz 1 "+ft+" 9\r\nHello1234\r\n", "NOT_STORED\r\n")
    check_echo("append xyz 1 12345 5\r\nHello\r\n", "NOT_STORED\r\n")
    check_echo("prepend xyz 1 12345 5\r\nHello\r\n", "NOT_STORED\r\n")
    test_time()
    test_time_2()

mc.kill()



